namespace MiskolcziRichard_WD7UVN;

static class Globals
{
	/*
	  Valójában célravezetőbb lenne Queue és Stack típust használni, de akkor nem
	  valósítanék meg egyetlen tanult adattípust sem, szóval marad a láncolt lista
	*/

	public static HetLista Teljesitett = new HetLista();
	public static HetLista Jovobeli = new HetLista();
    public static Verseny? Cel = null;
}
