namespace MiskolcziRichard_WD7UVN;

class MentesKezelo
{
    private readonly string CONFIG_PATH;
    private string COMPLETED_PATH;
    private string FUTURE_PATH;
    private string ATHLETE_PATH;

    private const string DEFAULT_CONFIG_PATH = "./.savepath.cfg";
    private const string DEFAULT_COMPLETED_PATH = "./.teljesitett.cfg";
    private const string DEFAULT_FUTURE_PATH = "./.jovobeli.cfg";
    private const string DEFAULT_ATHLETE_PATH = "./.versenyzo.cfg";

    public delegate void SendReport(string message);
    public event SendReport? Report;
    public event SendReport? Error;

    private void Beolvasas(string filePath)
    {
        //A versenyző adatait olvassa be

        if (!File.Exists(filePath))
		{
			throw new FileNotFoundException($"A megadott fájl ({filePath}) nem létezik!");
		}
        else
		{
			StreamReader sr = new StreamReader(filePath);

			try
			{
				while (!sr.EndOfStream)
				{
					string[] fields = sr.ReadLine().Split('=');
					
					switch (fields[0])
					{
						case "UszasIdo":
                            Versenyzo.UszasIdo = Convert.ToDouble(fields[1]);
                            break;
						case "UszasTav":
                            Versenyzo.UszasTav = Convert.ToDouble(fields[1]);
							break;
						case "KerekparIdo":
                            Versenyzo.KerekparIdo = Convert.ToDouble(fields[1]);
							break;
						case "KerekparTav":
                            Versenyzo.KerekparTav = Convert.ToDouble(fields[1]);
							break;
						case "FutasIdo":
                            Versenyzo.FutasIdo = Convert.ToDouble(fields[1]);
							break;
						case "FutasTav":
                            Versenyzo.FutasTav = Convert.ToDouble(fields[1]);
							break;
					}
				}

                sr.Close();
                Report?.Invoke("A betöltés sikeresen befejeződött");
            } catch (FormatException)
			{
				throw new HibasKonfiguracioKivetel("A versenyző adatai csak szám formátumban lehetnek rögzítve!");
			} catch (NullReferenceException)
			{
				throw new HibasKonfiguracioKivetel("Üres mezőket nem tartalmazhat a mentési fájl!");
			}
		}
    }
	
    private void Beolvasas(string filePath, ref HetLista lista)
	{
		if (!File.Exists(filePath))
		{
			throw new FileNotFoundException($"A megadott fájl ({filePath}) nem létezik!");
		}
        else
        {
			StreamReader sr = new StreamReader(filePath);
			HetLista hetek = new HetLista();
			
			try
			{
				LancoltLista<Nap> napok = new LancoltLista<Nap>();
				LancoltLista<Edzes> edzesek = new LancoltLista<Edzes>();

				while (!sr.EndOfStream)
				{
					string line = sr.ReadLine();
					if (line != "-")
					{
						string sportnem = line.Split(' ')[0];
						Edzes edzes;
						
						switch (sportnem)
						{
							case "Kerékpár":
								edzes = new Kerekpar(line);
								break;
								
							case "Futás":
								edzes = new Futas(line);
								break;
								
							case "Úszás":
								edzes = new Uszas(line);
								break;
								
							default:
								throw new HibasFajlKivetel($"Hibás bemeneti fájl:\n\t{sportnem[0]} nevű edzés osztály nem létezik!");
						}
						
						edzesek.VegereBeszuras(edzes);
					}
					else
					{
						switch (edzesek.Length)
						{
							case 1:
								napok.VegereBeszuras(new Nap(edzesek[0]));
                                edzesek = new LancoltLista<Edzes>();
								break;
							case 2:
								napok.VegereBeszuras(new Nap(edzesek[0], edzesek[1]));
                                edzesek = new LancoltLista<Edzes>();
								break;
							default:
								throw new HibasFajlKivetel($"Hibás bemeneti fájl:\n\tKéptelen edzésterv észlelve. Ellenőrizd a szeparátorok (\"-\") helyzetét!");
						}
						
						if (napok.Length == 7)
						{
							hetek.VegereBeszuras(new Het(napok.ToArray));
                            napok = new LancoltLista<Nap>();
						}
					}
				}

                if (napok.Length > 0)
                {
					hetek.VegereBeszuras(new Het(napok.ToArray));
                }

                lista = hetek;
                sr.Close();
                Report?.Invoke("A betöltés sikeresen befejeződött");
			} catch (FormatException)
			{
				throw new HibasKonfiguracioKivetel("Az edzési adatok hibás formátumban vannak rögzítve!");
			} catch (NullReferenceException)
			{
				throw new HibasKonfiguracioKivetel("Üres mezőket nem tartalmazhat a mentési fájl!");
			}
        }
	}
	
    public void KonfiguraciosFajlBetoltes(string filePath)
	{
		if (!File.Exists(filePath))
		{
			throw new KonfiguracioNemLetezikKivetel($"A megadott fájl ({filePath}) nem létezik!");
		}
        else
        {
            StreamReader sr = new StreamReader(filePath);

            while (!sr.EndOfStream)
            {
                string[] fields = sr.ReadLine().Split('=');

                switch (fields[0]) //a lista neve
                {
                    case "Teljesitett":
                        COMPLETED_PATH = fields[1];
                        Beolvasas(COMPLETED_PATH, ref Globals.Teljesitett);
                        break;
                    case "Jovobeli":
                        FUTURE_PATH = fields[1];
                        Beolvasas(FUTURE_PATH, ref Globals.Jovobeli);
                        break;
                    case "Cel":
                        if (fields[1] == "Sprint")
                            Globals.Cel = new Sprint();
                        else if (fields[1] == "Rovid")
                            Globals.Cel = new Rovid();
                        else if (fields[1] == "Kozep")
                            Globals.Cel = new Kozep();
                        else if (fields[1] == "Hosszu")
                            Globals.Cel = new Hosszu();
                        else
                            Globals.Cel = null;

                        break;
                    case "Versenyzo":
                        ATHLETE_PATH = fields[1];
                        Beolvasas(ATHLETE_PATH);
                        break;
                    default:
                        throw new HibasKonfiguracioKivetel($"Váratlan beállítás: {fields[0]}");
                }
            }

            sr.Close();
            Report?.Invoke("A betöltés sikeresen befejeződött");
        }
    }

    public void Mentes()
    {
		Mentes(ATHLETE_PATH);
		Mentes(COMPLETED_PATH, Globals.Teljesitett);
		Mentes(FUTURE_PATH, Globals.Jovobeli);
    }

    private void Mentes(string filePath)
	{
        StreamWriter sw = new StreamWriter(filePath);

        sw.WriteLine($"UszasIdo={Versenyzo.UszasIdo}");
        sw.WriteLine($"UszasTav={Versenyzo.UszasTav}");
        sw.WriteLine($"KerekparIdo={Versenyzo.KerekparIdo}");
        sw.WriteLine($"KerekparTav={Versenyzo.KerekparTav}");
        sw.WriteLine($"FutasIdo={Versenyzo.FutasIdo}");
        sw.WriteLine($"FutasTav={Versenyzo.FutasTav}");

        sw.Close();
        Report?.Invoke("A mentés sikeresen befejeződött");
    }

    public void Mentes(string filePath, HetLista lista)
    {
        StreamWriter sw = new StreamWriter(filePath);

        for (int i = 0; i < lista.Length; i++)
        {
            for (int j = 0; j < lista[i].Length; j++)
            {
                for (int k = 0; k < lista[i][j].Length; k++)
                {
                    Edzes tmp = lista[i][j][k];
                    sw.WriteLine(tmp.SaveOutput);
                }
			    sw.WriteLine("-");
            }
        }

        sw.Close();
        Report?.Invoke("A mentés sikeresen befejeződött");
	}

    private void KonfiguraciosFajlLetrehozasa()
    {
        StreamWriter sw = new StreamWriter(CONFIG_PATH);

        sw.WriteLine($"Teljesitett={DEFAULT_COMPLETED_PATH}");
        sw.WriteLine($"Jovobeli={DEFAULT_FUTURE_PATH}");
        sw.WriteLine($"Versenyzo={DEFAULT_ATHLETE_PATH}");

        sw.Close();
    }

    public MentesKezelo(string[] args)
    {
        FUTURE_PATH = DEFAULT_FUTURE_PATH;
		COMPLETED_PATH = DEFAULT_COMPLETED_PATH;
        ATHLETE_PATH = DEFAULT_ATHLETE_PATH;

        if (args.Length == 0)
        {
            CONFIG_PATH = DEFAULT_CONFIG_PATH;
        }
        else if (args.Length == 1)
        {
            CONFIG_PATH = args[0];
        }
        else
        {
            Help();
        }
    }

    public void Init()
    {
		try
		{
			KonfiguraciosFajlBetoltes(CONFIG_PATH);
		}
        catch (FileNotFoundException e)
        {
            try
            {
                Mentes(DEFAULT_ATHLETE_PATH);
                Mentes(DEFAULT_COMPLETED_PATH, Globals.Teljesitett);
                Mentes(DEFAULT_FUTURE_PATH, Globals.Jovobeli);
            }
            catch (Exception)
            {
                throw e;
            }
        }
		catch (KonfiguracioKivetel e)
		{
            try
            {
                KonfiguraciosFajlLetrehozasa();
				KonfiguraciosFajlBetoltes(CONFIG_PATH);
            } catch (FileNotFoundException f)
            {
                try 
                {
                    Mentes(DEFAULT_ATHLETE_PATH);
                    Mentes(DEFAULT_COMPLETED_PATH, Globals.Teljesitett);
                    Mentes(DEFAULT_FUTURE_PATH, Globals.Jovobeli);
                }
                catch (Exception)
                {
                    throw f;
                }
            } catch (Exception)
            {
                throw e; //nem sikerült korrigálni a hibát
            }
        }
        catch (Exception e)
		{
			Error?.Invoke(e.Message);
            throw e;
        }
	}
	
    private void Help()
    {
		Error?.Invoke("Opciók:\n\tnincs paraméter: az alapértelmezett elérési útvonalból olvassa be a konfigurációs fájlt\n\tegy paraméter: a paraméterül megadott helyről próbál meg érvényes konfigurációt beolvasni");
	}
}
