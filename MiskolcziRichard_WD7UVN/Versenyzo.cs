namespace MiskolcziRichard_WD7UVN;

static class Versenyzo
{
	// m/s mértékegységben
    public static double FutasIdo;
    public static double UszasIdo;
    public static double KerekparIdo;
    public static double FutasTav;
    public static double UszasTav;
    public static double KerekparTav;

    public static bool IsValid
    {
        get 
        {
            if (
                FutasIdo == 0 &&
                UszasIdo == 0 &&
                KerekparIdo == 0 &&
                FutasTav == 0 &&
                UszasTav == 0 &&
                KerekparTav == 0
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public static string ToString()
    {
        string output = String.Empty;
        output += "Versenyző adatai:\n";
        output += $"-\tMaximum futás táv: {FutasTav}\n";
        output += $"-\tMaximum futás idő: {FutasIdo}\n";
        output += "\n";
        output += $"-\tMaximum kerékpár táv: {KerekparTav}\n";
        output += $"-\tMaximum kerékpár idő: {KerekparIdo}\n";
        output += "\n";
        output += $"-\tMaximum úszás táv: {UszasTav}\n";
        output += $"-\tMaximum úszás idő: {UszasIdo}\n";

        return output;
    }
}
