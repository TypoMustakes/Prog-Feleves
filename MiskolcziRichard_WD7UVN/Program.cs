﻿namespace MiskolcziRichard_WD7UVN;

partial class Program
{
	static int Main(string[] args)
	{
        MentesKezelo mentesKezelo = new MentesKezelo(args);
		//mentesKezelo.Report += (string message) => Console.WriteLine(message);
		mentesKezelo.Error += (string message) => Console.WriteLine(message);

        try
        {
			mentesKezelo.Init();
			UserInterface.Start(Eldontendo, Console.WriteLine, Console.ReadLine);
        } catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return 1;
        }

        mentesKezelo.Mentes();

        return 0;
    }

    public static bool Eldontendo(string message)
    {
        Console.Write($"{message}\n(true/false): ");

        while (true)
        {
            try
            {
                bool input = Convert.ToBoolean(Console.ReadLine());
                if (input == null)
                {
                    throw new NullReferenceException("Csak a fent felsorolt opciók egyikét írhatja válaszul");
                }
                else
                {
                    return input;
                }
            }
            catch (FormatException)
            {
                throw new NullReferenceException("Csak a fent felsorolt opciók egyikét írhatja válaszul");
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    public static string EdzesBeolvasas()
    {
        string output = String.Empty;

        bool flag = false;
        while (!flag)
        {
            Console.Write("Milyen nemű edzést végeztél ma?\n(Úszás/Futás/Kerékpár): ");
            switch (Console.ReadLine().ToLower())
            {
                case "úszás":
					output += "Úszas ";
                    Common();
                    Uszas();
                    flag = true;
                    break;
                case "futás":
					output += "Futás ";
                    Common();
                    Futas();
                    flag = true;
                    break;
                case "kerékpár":
					output += "Kerékpár ";
                    Common();
                    Kerekpar();
                    flag = true;
                    break;
                default:
                    Console.WriteLine("Csak a megadott opciók közül választhatsz!");
                    break;
            }
        }

        return output;

        void Common()
        {
            bool flag = false;
            while (!flag)
            {
                try
                {
                    Console.Write("Megtett táv (m): ");
                    int tmp = Convert.ToInt32(Console.ReadLine()); //hátha van exception
                    output += $"{tmp} ";

                    Console.Write("Időtartam (perc): ");
                    tmp = Convert.ToInt32(Console.ReadLine());
                    output += $"{tmp} ";

                    Console.Write("Elégetett kalória (kcal): ");
                    tmp = Convert.ToInt32(Console.ReadLine());
                    output += $"{tmp} ";

                    flag = true;
                }
                catch (FormatException)
				{
                    Console.WriteLine("Megengedett bemeneti formátum: szám");
                }
            }
		}

        void Kerekpar()
        {
            bool flag = false;
            while (!flag)
            {
                try
                {
                    Console.Write("Pulzusszám (bpm): ");
                    int tmp = Convert.ToInt32(Console.ReadLine()); //hátha van exception
                    output += $"{tmp} ";

                    Console.Write("Pedálfordulat (rpm): ");
                    tmp = Convert.ToInt32(Console.ReadLine());
                    output += $"{tmp}";

                    flag = true;
                }
                catch (FormatException)
				{
                    Console.WriteLine("Megengedett bemeneti formátum: szám");
                }
            }
        }

        void Uszas()
        {
            bool flag = false;
            while (!flag)
            {
                try
                {
                    Console.Write("Úszásnem\n(Gyors/Pillangó/Mell/Hát): ");
                    string tmp = Console.ReadLine().ToLower();

                    if (tmp == "gyors" ||
                        tmp == "pillangó" ||
                        tmp == "mell" ||
                        tmp == "hát")
                    {
                        output += $"{tmp} ";
						flag = true;
                    }
                    else
                    {
						throw new FormatException();
					}
                }
                catch (FormatException)
				{
                    Console.WriteLine("Megengedett bemeneti formátum: szám");
                }
            }
		}

        void Futas()
        {
            bool flag = false;
            while (!flag)
            {
                try
                {
                    Console.Write("Pulzusszám (bpm): ");
                    int tmp = Convert.ToInt32(Console.ReadLine()); //hátha van exception
                    output += $"{tmp} ";

                    flag = true;
                }
                catch (FormatException)
				{
                    Console.WriteLine("Megengedett bemeneti formátum: szám");
                }
            }
		}
    }
}
