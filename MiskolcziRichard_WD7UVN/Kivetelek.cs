namespace MiskolcziRichard_WD7UVN;

class NincsMegoldasKivetel : Exception
{
	public NincsMegoldasKivetel() : base() { }
	public NincsMegoldasKivetel(string message) : base(message) { }
	public NincsMegoldasKivetel(string message, Exception inner) : base(message, inner) { }
}

class HibasFajlKivetel : Exception
{
	public HibasFajlKivetel() : base() { }
	public HibasFajlKivetel(string message) : base(message) { }
	public HibasFajlKivetel(string message, Exception inner) : base(message, inner) { }
}

class KonfiguracioKivetel : Exception
{
	public KonfiguracioKivetel() : base() { }
	public KonfiguracioKivetel(string message) : base(message) { }
	public KonfiguracioKivetel(string message, Exception inner) : base(message, inner) { }
}

class HibasKonfiguracioKivetel : KonfiguracioKivetel
{
	public HibasKonfiguracioKivetel() : base() { }
	public HibasKonfiguracioKivetel(string message) : base(message) { }
	public HibasKonfiguracioKivetel(string message, Exception inner) : base(message, inner) { }
}

class KonfiguracioNemLetezikKivetel : KonfiguracioKivetel
{
	public KonfiguracioNemLetezikKivetel() : base() { }
	public KonfiguracioNemLetezikKivetel(string message) : base(message) { }
	public KonfiguracioNemLetezikKivetel(string message, Exception inner) : base(message, inner) { }
}
