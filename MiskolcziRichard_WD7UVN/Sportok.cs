namespace MiskolcziRichard_WD7UVN;

public abstract class Edzes
{
	public double Tavolsag { get; }
	public double Idotartam { get; }
	public double Kaloria { get; }
	public abstract string SportAg { get; }

    public Edzes(double Tavolsag, double Idotartam, double Kaloria)
	{
		this.Tavolsag = Tavolsag;
		this.Idotartam = Idotartam;
		this.Kaloria = Kaloria;
	}

	public Edzes(string input)
	{
		try
		{
			string[] mezok = input.Split(' ');
			//a 0. elem az osztaly neve
			this.Tavolsag = Convert.ToInt32(input[1]);
			this.Idotartam = Convert.ToInt32(input[2]);
			this.Kaloria = Convert.ToInt32(input[3]);
		} catch (FormatException)
		{
			throw new ArgumentException();
		}
	}
	
	public abstract override string ToString();
    public abstract string SaveOutput { get; } //kesobb konnyeden beolvashato formatumban kihany magarol minden adatot
}

public sealed class Kerekpar : Edzes
{
	public override string SportAg
    {
        get
        {
            return "Kerékpár";
        }
    }

    public int PulzusSzam { get; }
	public int PedalFordulat { get; } //mértékegysége: rpm

    public Kerekpar() : base(Versenyzo.KerekparTav, Versenyzo.KerekparTav, 300)
    {
        Random rnd = new Random();
        this.PulzusSzam = rnd.Next(120, 191);
        this.PedalFordulat = rnd.Next(60, 100);
    }

    public Kerekpar(
		int PulzusSzam,
		int PedalFordulat,
		int Tavolsag,
		int Idotartam,
		int Kaloria
	) : base(
		Tavolsag,
		Idotartam,
		Kaloria
	)
	{
		this.PulzusSzam = PulzusSzam;
		this.PedalFordulat = PedalFordulat;
	}

	public Kerekpar(string input) : base(input)
	{
		try
		{
			string[] mezok = input.Split(' ');
			this.PulzusSzam = Convert.ToInt32(input[4]);
			this.PedalFordulat = Convert.ToInt32(input[5]);
		} catch (FormatException)
		{
			throw new ArgumentException();
		}
	}

	public override string SaveOutput
	{
        get
        {
            return $"{this.SportAg} {this.Tavolsag} {this.Idotartam} {this.Kaloria} {this.PulzusSzam} {this.PedalFordulat}";
        }
    }

	public override string ToString()
	{
		return $"{this.SportAg}:\n\t- Megtett táv: {this.Tavolsag} m\n\t- Időtartam: {this.Idotartam} perc\n\t- Elégetett kalória: {this.Kaloria} kcal\n\t- Pulzusszám: {this.PulzusSzam} bpm\n\t- Pedálfordulat: {this.PedalFordulat} rpm";
	}
}

public sealed class Futas : Edzes
{
	public override string SportAg
    {
        get
        {
            return "Futás";
        }
    }

    public int PulzusSzam { get; }

    public Futas() : base(Versenyzo.FutasTav, Versenyzo.FutasIdo, 300)
    {
        Random rnd = new Random();
        this.PulzusSzam = rnd.Next(120, 191);
    }

    public Futas(
		int PulzusSzam,
		int Tavolsag,
		int Idotartam,
		int Kaloria
	) : base(
		Tavolsag,
		Idotartam,
		Kaloria
	)
	{
		this.PulzusSzam = PulzusSzam;
	}
	
	public Futas(string input) : base(input) //input: a mentési fájlból beolvasott sor
	{
		try
		{
			this.PulzusSzam = Convert.ToInt32((input.Split(' '))[4]);
		} catch (FormatException)
		{
			throw new ArgumentException();
		}
	}
	
	public override string ToString()
	{
		return $"{this.SportAg}:\n\t- Megtett táv: {this.Tavolsag} m\n\t- Időtartam: {this.Idotartam} perc\n\t- Elégetett kalória: {this.Kaloria} kcal\n\t- Pulzusszám: {this.PulzusSzam} bpm";
	}

	//egy olyan formatumba kop ki magarol minden infot, amellyel kesobb a konstruktora segitsegevel letre tudja hozni onmagat
	public override string SaveOutput
	{
        get
        {
            return $"{this.SportAg} {this.Tavolsag} {this.Idotartam} {this.Kaloria} {this.PulzusSzam}";
        }
    }
}

public enum UszasNemek
{
	Gyors,
	Pillango,
	Mell,
	Hat
}

public sealed class Uszas : Edzes
{
	public override string SportAg
    {
        get
        {
            return "Úszás";
        }
    }

    public UszasNemek UszasNem { get; }

    /*
	  Alapbeállítás:
	  - 60 perces edzés
	  - 300 kalória
	  - Véletlenszerű úszásnem
	 */

    public Uszas() : base(Versenyzo.UszasTav, Versenyzo.UszasIdo, 300)
    {
        Random rnd = new Random();
		this.UszasNem = (UszasNemek)rnd.Next(0, 5);
    }

    public Uszas(
		UszasNemek UszasNem,
		int Tavolsag,
		int Idotartam,
		int Kaloria
	) : base(
		Tavolsag,
		Idotartam,
		Kaloria
	)
	{
		this.UszasNem = UszasNem;
	}

	public Uszas(string input) : base(input)
	{
		UszasNemek tmp;
		if (!Enum.TryParse((input.Split(' '))[4], out tmp))
		{
			throw new ArgumentException();
		}
		this.UszasNem = tmp;
	}

	public override string SaveOutput
	{
        get
        {
            return $"{this.SportAg} {this.Tavolsag} {this.Idotartam} {this.Kaloria} {this.UszasNem}";
        }
    }

	public override string ToString()
	{
		return $"{this.SportAg}:\n\t- Távolság: {this.Tavolsag} m\n\t- Időtartam: {this.Idotartam} perc\n\t- Elégetett kalória: {this.Kaloria} kcal\n\t- Úszasnem: {this.UszasNem}";
	}
}
