namespace MiskolcziRichard_WD7UVN;

static class UserInterface
{
    public delegate bool Prompt(string message);
    private static event Prompt TrueOrFalse;

    public delegate void Writer(string message);
    private static event Writer Output;

    public delegate string Reader();
    private static event Reader Input;

    public static void Start(Prompt trueorfalse, Writer output, Reader input)
    {
        TrueOrFalse += trueorfalse;
        Output += output;
        Input += input;
        Init();
    }

    private static void Init()
    {
        if (Globals.Cel == null)
        {
            NincsEdzesterv(); //ha ez így van, akkor az az edzésterv hagy némi kívánni valót
        }

        Nap? mai = null;

        try
        {
            mai = Globals.Jovobeli.Elso[0];
        } catch (IndexOutOfRangeException)
        {
            //hagyjuk rá. a következő lépésben null marad a 'mai' változó, és generálódik egy edzésterv
        }

        if (mai != null)
        {
            MaiKiiras();
            if (TrueOrFalse("Sikerült a fenti edzést/edzéseket teljesíteni?") == true)
            {
                Teljesitette();
            }
            else
            {
                NemTeljesitette();
            }
        }
        else //nincs edzésterv
        {
            NincsEdzesterv();
        }

        if (TrueOrFalse("Szeretné folytatni?") == true)
        {
            Init();
        }

        void MaiKiiras()
		{
            if (mai.Length == 1)
            {
                Output(mai[0].ToString());
            }
			else if (mai.Length == 2)
            {
                Output(mai[0].ToString());
                Output(mai[1].ToString());
			}
		}

        void Teljesitette()
        {
			Globals.Teljesitett.VegereBeszuras(mai);
			Globals.Jovobeli.Elso.Torles(mai);
        }

        void NemTeljesitette()
        {
			Globals.Jovobeli.Elso.Torles(mai);
			
			bool flag = false;
			while (!flag)
			{
				try
				{
					Output("Hány edzést teljesítettél ma?");
					Nap tmp;
					switch (Convert.ToInt32(Input()))
					{
						case 1:
							tmp = new Nap(EdzesEpito());
							Globals.Teljesitett.Utolso.VegereBeszuras(tmp);
							if (HatvanSzazalek(mai, tmp))
							{
                                Output("Így is sikerült teljesítened a kívánt célt!");
                                Bonuszok(tmp[0]);
                                Output(Versenyzo.ToString());
							}
							flag = true;
							break;
						case 2:
							tmp = new Nap(EdzesEpito(), EdzesEpito());
							Globals.Teljesitett.Utolso.VegereBeszuras(tmp);
							if (HatvanSzazalek(mai, tmp))
							{
                                Output("Így is sikerült teljesítened a kívánt célt!");
								Bonuszok(tmp[0]);
								Bonuszok(tmp[1]);
                                Output(Versenyzo.ToString());
                            }
							flag = true;
							break;
						default:
							throw new FormatException();
					}
				}
				catch (FormatException)
				{
					Output("Csak 1, vagy 2 lehet a válaszod!");
				}
			}
		}

        void NincsEdzesterv()
        {
            Globals.Jovobeli = new HetLista();
            try
            {
                if (!Versenyzo.IsValid)
                {
                    VersenyzoFelmeres();
                }

                if (Globals.Cel == null)
                {
                    VersenyValasztas();
                }

                Globals.Jovobeli.VegereBeszuras(EdzestervGenerator.Generalas());
            } catch (NincsMegoldasKivetel)
            {
                Output("Nem sikerült edzéstervet generálni. Próbáld újra!");
            }
		}

        void VersenyValasztas()
        {
            bool flag = false;

            while (!flag)
            {
                try
                {
                    Output("1.) Sprint\n2.) Rövid\n3.) Közép\n4.) Hosszú\nA fenti versenyek közül tűzzön ki egyet célnak!\n(szám): ");
                    switch (Input())
                    {
                        case "1":
                            Globals.Cel = new Sprint();
                            flag = true;
                            break;
                        case "2":
                            Globals.Cel = new Rovid();
							flag = true;
                            break;
                        case "3":
                            Globals.Cel = new Kozep();
							flag = true;
                            break;
                        case "4":
                            Globals.Cel = new Hosszu();
							flag = true;
                            break;
                        default:
                            throw new FormatException();
                    }
                }
                catch (FormatException)
                {
                    Output("Csak az 1, 2, 3 vagy 4 opciót adhatja válaszul!");
                }
            }
        }

        void VersenyzoFelmeres()
        {
            bool flag = false;
            while (!flag)
            {
                try
                {
                    Output("Mennyi ideig tud futni?\n(szám): ");
                    Versenyzo.FutasIdo = Convert.ToInt32(Input());

                    Output("Mekkora távot tud lefutni?\n(szám): ");
                    Versenyzo.FutasTav = Convert.ToInt32(Input());

                    Output("Mennyi ideig tud kerékpározni?\n(szám): ");
                    Versenyzo.FutasIdo = Convert.ToInt32(Input());

                    Output("Mekkora távot tud kerékpárral megtenni?\n(szám): ");
                    Versenyzo.FutasTav = Convert.ToInt32(Input());

                    Output("Mennyi ideig tud úszni?\n(szám): ");
                    Versenyzo.FutasIdo = Convert.ToInt32(Input());

                    Output("Mekkora távot tud leúszni?\n(szám): ");
                    Versenyzo.FutasTav = Convert.ToInt32(Input());

                    flag = true;
                }
                catch (FormatException)
                {
                    Output("Csak szám lehet a válasza!");
                }
            }
        }
    }

    private static void Bonuszok(Edzes edzes)
    {
		switch (edzes.SportAg)
		{
			case "Úszás":
				Versenyzo.UszasTav = Versenyzo.UszasTav * 1.1;
				Versenyzo.UszasIdo = Versenyzo.UszasIdo * 1.01;
				break;
			case "Kerékpár":
				Versenyzo.KerekparTav = Versenyzo.KerekparIdo * 1.1;
				Versenyzo.KerekparIdo = Versenyzo.KerekparIdo * 1.01;
				break;
			case "Futás":
				Versenyzo.FutasTav = Versenyzo.FutasTav * 1.1;
				Versenyzo.FutasIdo = Versenyzo.FutasIdo * 1.01;
				break;
		}
	}

    private static bool HatvanSzazalek(Nap nap1, Nap nap2)
    {
        return ((nap2.OsszIdo + nap2.OsszTav) >= (nap1.OsszIdo + nap1.OsszTav) * 0.6) ? true : false;
    }

    private static Edzes EdzesEpito()
    {
        Output("Milyen sportot űztél?");

        while (true)
        {
            string tmp = Input().ToLower();
            string sportnem = tmp.Split(' ')[0];
            switch (sportnem)
            {
			    case "úszás":
                    return UszasEpito();
                case "futás":
                    return FutasEpito();
			    case "kerékpár":
                    return KerekparEpito();
			    default:
                    Output("Ilyen nevű sportágot nem ismerek :(\nKérlek a \"Kerékpár\", \"Úszás\" vagy \"Futás\" opciók közül válassz!");
                    break;
            }
        }

        Uszas UszasEpito()
        {
            while (true)
            {
                try 
                {
                    Output("Mekkora távot úsztál le? (méterben)");
                    int tav = Convert.ToInt32(Input());

                    Output("Mennyi idő alatt úsztad le ezt a távot? (percben)");
                    int ido = Convert.ToInt32(Input());

                    Output("Hány kalóriát égettél el ezzel? (kcal-ban)");
                    int kcal = Convert.ToInt32(Input());

                    Output("Milyen úszásnemben úsztál? (mell/gyors/pillangó/hát)");
                    string tmp = Input().ToLower();
                    UszasNemek uszasNem = 0;

                    switch (tmp)
                    {
                        case "mell":
                            uszasNem = UszasNemek.Mell;
                            break;
                        case "gyors":
                            uszasNem = UszasNemek.Gyors;
                            break;
                        case "hát":
                            uszasNem = UszasNemek.Hat;
                            break;
                        case "pillangó":
                            uszasNem = UszasNemek.Pillango;
                            break;
                        default:
                            throw new FormatException();
                    }

                    return new Uszas(uszasNem, tav, ido, kcal);
                }
                catch (FormatException)
                {
                    Output("Valamit rosszul adtál meg!\nKérlek, hogy figyelj a zárójelben feltüntetett bemeneti formátumokra!");
                    continue;
                }
            }
        }

        Futas FutasEpito()
        {
            while (true)
            {
                try 
                {
                    Output("Mekkora távot futottál le? (méterben)");
                    int tav = Convert.ToInt32(Input());

                    Output("Mennyi idő alatt futottad le ezt a távot? (percben)");
                    int ido = Convert.ToInt32(Input());

                    Output("Hány kalóriát égettél el ezzel? (kcal-ban)");
                    int kcal = Convert.ToInt32(Input());

                    Output("Mennyi volt ezalatt az átlag pulzusszámod? (bpm-ben)");
                    int bpm = Convert.ToInt32(Input());

                    return new Futas(bpm, tav, ido, kcal);
                }
                catch (FormatException)
                {
                    Output("Valamit rosszul adtál meg!\nKérlek, hogy figyelj a zárójelben feltüntetett bemeneti formátumokra!");
                    continue;
                }
            }
        }

        Kerekpar KerekparEpito()
        {
            while (true)
            {
                try 
                {
                    Output("Mekkora távot tettél meg? (méterben)");
                    int tav = Convert.ToInt32(Input());

                    Output("Mennyi idő alatt tetted meg ezt a távot? (percben)");
                    int ido = Convert.ToInt32(Input());

                    Output("Hány kalóriát égettél el ezzel? (kcal-ban)");
                    int kcal = Convert.ToInt32(Input());

                    Output("Mennyi volt ezalatt az átlag pulzusszámod? (bpm-ben)");
                    int bpm = Convert.ToInt32(Input());

                    Output("Mennyi volt az átlagos pedálfordulatszámod? (rpm-ben)");
                    int rpm = Convert.ToInt32(Input());

                    return new Kerekpar(bpm, rpm, tav, ido, kcal);
                }
                catch (FormatException)
                {
                    Output("Valamit rosszul adtál meg!\nKérlek, hogy figyelj a zárójelben feltüntetett bemeneti formátumokra!");
                    continue;
                }
            }
        }
    }
}
