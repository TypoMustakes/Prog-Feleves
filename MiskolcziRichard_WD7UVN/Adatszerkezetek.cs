namespace MiskolcziRichard_WD7UVN;

public sealed class Nap
{
	private Edzes ElsoEdzes;
	private Edzes? MasodikEdzes;
	public int Length;

    public double OsszTav;
    public double OsszIdo;

    public Edzes this[int i]
	{
		get
		{
			if (i == 0)
			{
				return ElsoEdzes;
			}
			else if (i == 1)
			{
				if (MasodikEdzes != null)
				{
					return MasodikEdzes;
				}
				else
				{
					throw new IndexOutOfRangeException("Erre a napra nincs ütemezve második edzés.");
				}
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		set
		{
			if (i == 0)
			{
				ElsoEdzes = value;
				Osszegzes();
			}
			else if (i == 1)
			{
				MasodikEdzes = value;
				Osszegzes();
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
	}

	public Nap(Edzes edzes1, Edzes edzes2 = null)
	{
		this.ElsoEdzes = edzes1;
		this.MasodikEdzes = edzes2;

		if (edzes2 != null)
		{
			this.Length = 2;
		}
		else
		{
			this.Length = 1;
		}

        Osszegzes();
    }

    private void Osszegzes()
    {
        this.OsszTav = ElsoEdzes.Tavolsag;
        this.OsszIdo = ElsoEdzes.Idotartam;

        if (MasodikEdzes != null)
        {
            this.OsszTav += MasodikEdzes.Tavolsag;
            this.OsszIdo += MasodikEdzes.Idotartam;
        }
    }
}

public sealed class Het
{
    private LancoltLista<Nap> Napok = new LancoltLista<Nap>();

    public double LefutottTav;
	public double LetekertTav;
	public double LeuszottTav;

	public int UszasokSzama;
	public int FutasokSzama;
	public int TekeresekSzama;

    public Het(Nap[] Napok)
	{
        foreach (Nap i in Napok)
        {
            this.Napok.VegereBeszuras(i);
        }

        Frissites();
    }

	public int Length
    {
        get
        {
            return this.Napok.Length;
        }
    }

    public Nap this[int index]
    {
        get
        {
            return this.Napok[index];
        }
        set
        {
            this.Napok[index] = value;
        }
    }

    public void VegereBeszuras(Nap nap)
    {
        this.Napok.VegereBeszuras(nap);
    }

    public void ElejereBeszuras(Nap nap)
    {
        this.Napok.ElejereBeszuras(nap);
    }

    public void Torles(Nap nap)
    {
        this.Napok.Torles(nap);
    }

    private void Frissites()
    {
        this.LefutottTav = 0;
		this.LeuszottTav = 0;
		this.LetekertTav = 0;

		this.UszasokSzama = 0;
		this.FutasokSzama = 0;
		this.TekeresekSzama = 0;

		for (int i = 0; i < this.Napok.Length; i++)
		{
			Nap ma = this.Napok[i];
			for (int j = 0; j < ma.Length; j++)
			{
				if (ma[j] is Futas)
				{
					this.LefutottTav += ma[j].Tavolsag;
					this.FutasokSzama++;
				}
				else if (ma[j] is Uszas)
				{
					this.LeuszottTav += ma[j].Tavolsag;
					this.UszasokSzama++;
				}
				else if (ma[j] is Kerekpar)
				{
					this.LetekertTav += ma[j].Tavolsag;
					this.TekeresekSzama++;
				}
			}
		}
	}
}

public delegate void Altalanos();
public class LancoltLista<T>
{
	public class ListaElem
	{
		public T ertek;
		public ListaElem? kovetkezo;
	}

	public T this[int i]
	{
		get
		{
			return Get(i).ertek;
		}
		set
		{
			Set(i, value);
		}
	}
	
	public event Altalanos? TartalomMegvaltozott;

	private ListaElem? fej;

    public int Length
    {
        get
        {
            int szam = 0;
            Bejaras((T) => { szam++; });
            return szam;
        }
    }

    public T[] ToArray
    {
        get
        {
            T[] arr = new T[this.Length];
            for (int i = 0; i < this.Length; i++)
            {
                arr[i] = this[i];
            }
            return arr;
        }
    }

    private ListaElem Get(int index)
	{
		ListaElem i = fej;
		int iter = 0;

		if (index < 0 || index >= this.Length)
		{
			throw new IndexOutOfRangeException();
		}
		else if (index == 0)
		{
			return fej;
		}
		else
		{
			while (iter < index)
			{
				i = i.kovetkezo;
				iter++;
			}
		}

		return i;
	}

	private void Set(int index, T ertek)
	{
		this[index] = ertek;
		TartalomMegvaltozott?.Invoke();
	}

	public void Torles(T elem)
	{
		ListaElem? p = this.fej;
		ListaElem? e = null;

		//amíg a lista végére nem érünk és meg nem találjuk a törlendő elemet
		while (p != null && !p.ertek.Equals(elem))
		{
			e = p;
			p = p.kovetkezo;
		}

		if (p != null)
		{
			if (e == null) //ha ez igy van, akkor a torlendo elem epp a fej kell legyen
			{
				this.fej = this.fej.kovetkezo;
			}
			else
			{
				e.kovetkezo = p.kovetkezo; //a ketto kozott levo emelet kilancoljuk
			}
		}

		TartalomMegvaltozott?.Invoke();
	}

	public delegate void Muvelet(T elem);

	public void Bejaras(Muvelet m)
	{
		ListaElem? p = this.fej;
		while (p != null)
		{
			m(p.ertek);
			p = p.kovetkezo;
		}
	}

	public void ElejereBeszuras(T elem)
	{
		ListaElem uj = new ListaElem
		{
			ertek = elem,
			kovetkezo = this.fej
		};
		this.fej = uj;

		TartalomMegvaltozott?.Invoke();
	}

	public void VegereBeszuras(T elem)
	{
		ListaElem uj = new ListaElem
		{
			ertek = elem,
			kovetkezo = null
		};

		if (this.fej == null)
		{
			this.fej = uj; //beszurando elem lesz a fej
		}
		else
		{
			ListaElem p = this.fej;
			while (p.kovetkezo != null) //addig lepkedunk, amig meg van kovetkezo elem
			{
				p = p.kovetkezo;
			}
			p.kovetkezo = uj; //az utolso elem referenciaja lesz a beszurando elem
		}

		TartalomMegvaltozott?.Invoke();
	}
}

public class HetLista : LancoltLista<Het>
{
    public HetLista()
    {
		this.TartalomMegvaltozott += HetekbeOsztas;
	}

    private void HetekbeOsztas()
    {
        Het utolso = this[this.Length - 1];
        if (utolso.Length > 7)
		{
			LancoltLista<Nap> tmp = new LancoltLista<Nap>();
			for (int i = utolso.Length - 1; utolso.Length > 7; i--)
			{
				tmp.VegereBeszuras(utolso[i]);
				utolso.Torles(utolso[i]);
			}
			Het uj = new Het(tmp.ToArray);
			this.VegereBeszuras(uj);
		}
	}

    public void VegereBeszuras(Nap nap)
    {
        Utolso.VegereBeszuras(nap);
        HetekbeOsztas();
    }

	public Het Utolso
    {
        get
        {
            return this[this.Length - 1];
        }
    }

	public Het Elso
    {
        get
        {
            return this[0];
        }
    }
}
