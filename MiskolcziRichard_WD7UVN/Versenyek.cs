namespace MiskolcziRichard_WD7UVN;

abstract class Verseny
{
    public readonly int UszasTav, KerekparTav, FutasTav;

    public Verseny(int UszasTav, int KerekparTav, int FutasTav)
    {
        this.UszasTav = UszasTav;
        this.KerekparTav = KerekparTav;
        this.FutasTav = FutasTav;
    }
}

sealed class Sprint : Verseny
{
    public Sprint() : base(750, 20000, 5000) { }
}

sealed class Rovid : Verseny
{
	public Rovid() : base(1500, 40000, 10000) { }
}

sealed class Hosszu : Verseny
{
    public Hosszu() : base(3800, 180000, 42000) { }
}

sealed class Kozep : Verseny
{
    public Kozep() : base(1900, 90000, 21000) { }
}
