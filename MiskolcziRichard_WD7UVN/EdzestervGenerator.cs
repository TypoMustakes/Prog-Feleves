namespace MiskolcziRichard_WD7UVN;

// Egyszerre egy hetet generál csak le a program
public static class EdzestervGenerator
{
    public static Het Generalas()
    {
        LancoltLista<Nap> napok = new LancoltLista<Nap>();

        Generalas(ref napok);

        return new Het(napok.ToArray);
    }

    private static void Generalas(ref LancoltLista<Nap> napok)
    {
        int i = 0;
        int? elozo = null;
        try
        {
            while (!Helyes(new Het(napok.ToArray)))
            {
                if (i < 7)
                {
                    if (elozo == null)
                    {
                        napok.VegereBeszuras(new Nap(new Uszas()));
                        elozo = 1;
                        i++;
                    }
                    else if (elozo == 1)
                    {
                        napok.VegereBeszuras(new Nap(new Futas()));
                        elozo = 2;
                        i++;
                    }
                    else if (elozo == 2)
                    {
                        napok.VegereBeszuras(new Nap(new Kerekpar()));
                        elozo = 3;
                        i++;
                    }
                    else if (elozo == 3)
                    {
                        napok.VegereBeszuras(new Nap(new Uszas()));
                        elozo = 1;
                        i++;
                    }
                }
                else
                {
                    /*
                      A példányosítás már megtörtént;
                      nem akarjuk felülírni a Nap objektumokat,
                      csak második edzést hozzáadni
                    */

                    if (elozo == null)
                    {
                        napok[i - 7][1] = new Uszas(); //visszatérünk az első napra, majd annak a második edzésére berakunk egy véletlenszerű úszást
                        elozo = 1;
                        i++;
                    }
                    else if (elozo == 3)
                    {
                        napok[i - 7][1] = new Uszas();
                        elozo = 1;
                        i++;
                    }
                    else if (elozo == 1)
                    {
                        napok[i - 7][1] = new Futas();
                        elozo = 2;
                        i++;
                    }
                    else if (elozo == 2)
                    {
                        napok[i - 7][1] = new Kerekpar();
                        elozo = 3;
                        i++;
                    }
                }
            }
        }
        catch (IndexOutOfRangeException)
        {
            throw new NincsMegoldasKivetel();
        }

		bool Helyes(Het het)
		{
			/*
			  Azt nem szükséges ellenőriznünk, hogy minden nap edzett-e a versenyző,
			  és hogy 2-nél többet edzett-e egy nap.
			  
			  Ez annak köszönhető, hogy a Nap osztály példányosításakor a konstruktorba
			  muszáj megadni legalább 1 nem-null Edzes típusú objektumot, 2-nél többet
			  pedig ha akarunk sem tudunk bejegyezni; az kivételt dobna.
			*/
			
			//erre sajnos nincs egyszerűbb diszjunktív normálforma, mert önmagában az
			if (het.UszasokSzama >= 2 && het.UszasokSzama <= 4 &&
				het.TekeresekSzama >= 2 && het.TekeresekSzama <= 4 &&
				het.UszasokSzama >= 2 && het.UszasokSzama <= 4 &&
				het.Length == 7)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
    }
}
